package com.grow.models;

public class BookingResponse {
    public BookingResult result;
    public int amount;

    public BookingResponse(BookingResult result){
        this.result = result;
        this.amount = 0;
    }

    public BookingResponse(BookingResult result, int amount){
        this.result = result;
        this.amount = amount;
    }
}
