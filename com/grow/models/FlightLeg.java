package com.grow.models;

public class FlightLeg {
    public City Start;
    public City End;
    public int EstimatedDurationInMinutes;

    public FlightLeg(City start, City end) {
        Start = start;
        End = end;
    }
}
