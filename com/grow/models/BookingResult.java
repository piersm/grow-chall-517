package com.grow.models;

public enum BookingResult {
    BOOKED,
    NOSEAT,
    ERROR
}
