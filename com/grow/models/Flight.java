package com.grow.models;

import java.util.Date;

public class Flight {
    public City Start;
    public City End;
    public Date TakeOff;
    public Date EstimatedArrival;
    public Jet jet;
}
