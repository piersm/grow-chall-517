package com.grow.models;

public enum SeatType {
    FirstClass,
    Business,
    Economy
}
