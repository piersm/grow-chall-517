package com.grow.models;

import com.grow.models.City;

public class BookingRequest {
    public int userId;
    public City source;
    public City destination;
    public SeatType seatType;
}
