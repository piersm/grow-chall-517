package com.grow.services;

import com.grow.models.City;
import com.grow.models.Graph;
import com.grow.models.Vertex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RouteConfiguration {

    private Graph routeGraph;

    public RouteConfiguration() {
        routeGraph = new Graph();
        routeGraph.addVertex('V', Arrays.asList(new Vertex('C', 100)));
        routeGraph.addVertex('C', Arrays.asList(new Vertex('V', 100), new Vertex('E', 50),new Vertex('T', 200)));
        routeGraph.addVertex('E', Arrays.asList(new Vertex('C', 50), new Vertex('W', 100)));
        routeGraph.addVertex('W', Arrays.asList(new Vertex('E', 100), new Vertex('T', 100), new Vertex('M', 100)));
        routeGraph.addVertex('T', Arrays.asList(new Vertex('W', 100), new Vertex('C', 200), new Vertex('M', 100)));
        routeGraph.addVertex('M', Arrays.asList(new Vertex('W', 100), new Vertex('T', 100)));
    }

    public List<City> getRouteBetweenCities(Character sourceId, Character destinationId){
        List<Character> cityIds = routeGraph.getShortestPath(sourceId, destinationId);
        List<City> cityRoute = new ArrayList<City>();
        for(Character cityId:cityIds) {
            cityRoute.add(getCityById(cityId));
        }
        cityRoute.add(getCityById(sourceId));
        return cityRoute;
    }

    private City getCityById(Character cityId){
        switch (cityId){
            case 'V':
                break;
            case 'E': new City("Edmonton");
                break;
            case 'T': new City("Toronto");
                break;
            case 'M': new City("Montreal");
                break;
            case 'C': new City("Calgary");
                break;
            case 'W': new City("Winnipeg");
                break;
        }
    }
}
