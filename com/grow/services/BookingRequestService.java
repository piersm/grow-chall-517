package com.grow.services;

import com.grow.models.*;
import com.grow.models.errors.NoSeatAvailableException;

public class BookingRequestService {

    private IRouteLocator routeLocator;
    private ITripService tripService;

    public BookingRequestService(
            IRouteLocator routeLocator,
            ITripService tripService){
        this.routeLocator = routeLocator;
        this.tripService = tripService;
    }

    public BookingResponse createBooking(BookingRequest request) {
        // validate request

        // find route
        Route route = routeLocator.getRoute(request.source, request.destination);
        BookingResult result;

        try {
            // find trip
            Trip trip = tripService.getTripByRoute(route);
            // book trip
            tripService.bookTrip(trip, request.userId);

            // bill flight
            return new BookingResponse(BookingResult.BOOKED, 1000);
        }catch(NoSeatAvailableException ex){
            return new BookingResponse(BookingResult.NOSEAT);
        }
    }
}
