package com.grow.services;

import com.grow.models.City;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RouteConfigurationTest {

    @org.junit.jupiter.api.Test
    void getRouteBetweenCities() {
        RouteConfiguration rc = new RouteConfiguration();
        List<City> results = rc.getRouteBetweenCities('V', 'M');
        for (City c: results) {
            System.out.println(c.Name);
        }
    }
}