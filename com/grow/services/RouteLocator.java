package com.grow.services;

import com.grow.models.City;
import com.grow.models.Route;

public class RouteLocator implements IRouteLocator {

    private RouteConfiguration routeConfig;

    public RouteLocator(RouteConfiguration routeConfig){
        this.routeConfig = routeConfig;
    }

    public Route getRoute(City source, City destination) {
        routeConfig.getRouteBetweenCities(source.Id, destination.Id);
        return null;
    }
}
