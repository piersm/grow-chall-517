package com.grow.services;

import com.grow.models.Route;
import com.grow.models.Trip;
import com.grow.models.errors.NoSeatAvailableException;

public interface ITripService {
    Trip getTripByRoute(Route route) throws NoSeatAvailableException;
    int bookTrip(Trip trip, int userId);
}
