package com.grow.services;

import com.grow.models.City;
import com.grow.models.Route;

public interface IRouteLocator {
    Route getRoute(City source, City destination);
}
