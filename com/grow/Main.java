package com.grow;

import com.grow.models.BookingResponse;
import com.grow.services.BookingRequestService;
import com.grow.services.RouteConfiguration;
import com.grow.services.RouteLocator;
import com.grow.services.TripService;

public class Main {

    public static void main(String[] args) {
        //bootstrap
        TripService tripService = new TripService();
        RouteLocator routeLocator = new RouteLocator(new RouteConfiguration());
        BookingRequestService service = new BookingRequestService(routeLocator, tripService);

        BookingResponse response = service.createBooking(null);

        System.out.println("Result: " + response.result);
        System.out.println("Amount: " + response.amount);
    }
}
